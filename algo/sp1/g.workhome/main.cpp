#include<iostream>
#include<string>

using namespace std;

// Вася реализовал функцию, которая переводит целое число из десятичной системы в двоичную. 
// Но, кажется, она получилась не очень оптимальной.

// Попробуйте написать более эффективную программу.
// Не используйте встроенные средства языка по переводу чисел в бинарное представление.

int main() {
    string out{};
    int in;

    cin >> in;

    if (in ==0 ){
       cout << "0" << endl; 

       return 0;
    }

    while (in > 0) {
        if (in%2 ==0) {
            out.push_back('0');
        } else {
             out.push_back('1');
        }
        
        
        in/= 2;
    }

    for (int i = out.size()-1; i >= 0; i--) {
        cout <<out[i];
    }

    cout << endl;

    return 0;
}