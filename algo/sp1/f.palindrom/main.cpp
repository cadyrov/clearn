#include<iostream>
#include<string>

using namespace std;

// Помогите Васе понять, будет ли фраза палиндромом. Учитываются только буквы и цифры, заглавные и
// строчные буквы считаются одинаковыми.
// Решение должно работать за O(N), где N — длина строки на входе.
// В единственной строке записана фраза или слово. Буквы могут быть только латинские. 

// Длина текста не превосходит 20000 символов.
// Фраза может состоять из строчных и прописных латинских букв, цифр, знаков препинания.
// Выведите «True», если фраза является палиндромом, и «False», если не является.
int main() {
    string word = "";

    cin >> word;

    int lidx=0, ridx = word.size();

    while (lidx < ridx) {
        if (
            word[lidx]<48 ||
            (word[lidx]>57 &&  word[lidx]<65) ||
            (word[lidx] > 90 && word[lidx]<97) ||
            (word[lidx] > 122)
        ) {
            lidx++;

            continue;
        }

        if (word[ridx]<48 ||(word[ridx]>57 &&  word[ridx]<65)  || (word[ridx] > 90 && word[ridx] < 97) ||(word[ridx] > 122)) {
            ridx--;
            
            continue;
        }
 
        if (word[lidx] != word[ridx] && abs(word[lidx] - word[ridx]) != 32 ) {
            cout << "False"s << endl;

            return 0;
        }

        lidx++;
        ridx--;
    }

    cout << "True"s << endl;

    return 0;
}