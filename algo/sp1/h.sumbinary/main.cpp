#include<iostream>
#include<string>

using namespace std;

// Формат ввода
// Два числа в двоичной системе счисления, каждое на отдельной строке. 
// Длина каждого числа не превосходит 10 000 символов.

// Формат вывода
// Одно число в двоичной системе счисления.

string sum(string big, string less) {
  int bi = big.size();
  int li = less.size();
  string out{};
  out.reserve(bi);
 
  int df = 0;

  while (li >0) {
    bi--;
    li--;
    if (less[li]=='0' && big[bi]=='0') {
        if (df ==0) {
            out.push_back('0');

            continue;
        }
        
        df =0;
        out.push_back('1');

        continue;
    }

    if (less[li]=='1' && big[bi]=='1') {
        if (df ==0) {
            df = 1;
            out.push_back('0');

            continue;
        }
        
        df = 1;
        out.push_back('1');

        continue;
    }

    if (df ==0) {
        out.push_back('1');

        continue;
    }

    out.push_back('0');
  }

  while (bi >0) {
    bi--;
    if (df == 0) {
        out.push_back(big[bi]);
    } else if (big[bi] == '0'){
        df=0;
        out.push_back('1');
    } else {
        out.push_back('0');
    }
  }

  if (df ==1) {
    out.push_back('1');
  }

  return out;
}

int main() {
    string a,b;
    string out;

    cin >> a >>b;

    if (a.size() == 0) {
        cout << b << endl;

        return 0;
    }

    if (b.size() == 0) {
        cout << a << endl;

        return 0;
    }

    if (a.size() > b.size()) {
        out = sum(a, b);
    } else {
        out = sum(b, a);
    }

    for (int i = out.size()-1;i >=0; i-- ) {
        cout << out[i];
    }

    cout <<endl;

    return 0;
}