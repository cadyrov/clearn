#include<iostream>
#include<vector>
#include <algorithm>

using namespace std;

// Дана матрица. Нужно написать функцию, которая для элемента возвращает всех его соседей. 
// Соседним считается элемент, находящийся от текущего на одну ячейку влево, вправо, вверх или вниз. 
// Диагональные элементы соседними не считаются.

// В первой строке задано n — количество строк матрицы.
// Во второй — количество столбцов m. Числа m и n не превосходят 1000. 
// В следующих n строках задана матрица. Элементы матрицы — целые числа, по модулю не превосходящие 1000. 
//В последних двух строках записаны координаты элемента, соседей которого нужно найти. Индексация начинается с нуля.

// Напечатайте нужные числа в возрастающем порядке через пробел.

vector<int> mtrx(const vector<vector<int>>& in, int x, int y) {
    vector<int> out;
    out.reserve(4);

    if (x > 0) {
        out.push_back(in[x-1][y]);
    }

    if (x < static_cast<int>(in.size()-1)) {
        out.push_back(in[x+1][y]);
    }
    
    if (y > 0) {
        out.push_back(in[x][y-1]);
    }

    if (y < static_cast<int>(in[x].size()-1)) {
        out.push_back(in[x][y+1]);
    }


    sort(out.begin(), out.end());

    return out;
} 

int main() {
    int n=0, m =0, x=0, y=0;
    
    cin >> n >> m;

    vector<vector<int>> matrix(n);
    vector<int> out;

    for (int i =0; i < n; i++) {
        vector<int> ix(m);

        for (int j = 0; j < m; j++) {
            cin>>ix[j];
        }

        matrix[i] = ix;
    }

    cin >> x >> y;

    out = mtrx(matrix, x, y);

    for (size_t i = 0; i < out.size(); i++) {
        if (i > 0) {
            cout << " "s;
        }

        cout << out[i];
    }

    cout << endl;
    
    return 0;
}