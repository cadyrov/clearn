#include<iostream>
#include<string>
#include<vector>
#include <algorithm>

using namespace std;


// Сортировка по убыванию
// Вам даны строки текстового файла. Отсортируйте набор этих строк по убыванию.

// Формат ввода
// Количество строк не превосходит 1000. Каждая строка состоит из символов ASCII с кодами от 32 до 126, 
// длина строки не превосходит 100.

// Формат вывода
// Напечатайте строки в отсортированном по убыванию порядке. Для сравнения строк друг с другом достаточно 
// использовать стандартные операторы сравнения, определённые для std::string.

int main(){
   string in = "";

   vector<string> out;

   while(cin >> in) {
      out.push_back(in);
   }

   sort(out.rbegin(), out.rend());
 
   for (size_t i = 0; i < out.size(); i++) {
      cout << out[i] << endl;
   }

   return 0;
}
